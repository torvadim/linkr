const SOCIAL_MEDIAS = {
  FACEBOOK: "facebook",
  INSTAGRAM: "instagram",
  TWITTER: "twitter",
};

module.exports = {
  SOCIAL_MEDIAS,
};
